
drop table if exists gmedia.nielsendata10102018;

create table gmedia.nielsendata10102018 (
Timestamp timestamp,
LogDate date,
NielsenId varchar (50),
DeviceType varchar (150),
ua varchar (300),
ipMD5 varchar (150),
Isp varchar (150),
Os varchar (50),
browser varchar (150),
lat float(10),
lon float(10),
SegmentId varchar(50)
);

drop table gmedia.nielsendata021018;

create table gmedia.nielsendata021018 (
Timestamp timestamp,
LogDate date,
NielsenId varchar (50),
DeviceType varchar (150),
ua varchar (300),
ipMD5 varchar (150),
Isp varchar (150),
Os varchar (50),
browser varchar (150),
lat float(10),
lon float(10),
SegmentId varchar(50)
);

drop table gmedia.IcecastData021018;
create table gmedia.IcecastData021018(
RequestIPAddress	varchar (20),
RequestTime	timestamp,
RequestType	varchar(10),
Station	varchar (100),
QueryTime	varchar (20),
Status varchar (10),
Lat float (10),
Lon	float (10),
Channel	varchar (50),
Kruxid	varchar (100),
adswizz	varchar (100),
PlayerId	varchar (50),
UserAgent varchar (300),	
Device varchar (100),
ListenerId	varchar (100),
ListenerId2	varchar (100),
Kuid	varchar (50),
skey	varchar (50),
Guid	varchar (100),
IcecastServer	varchar (50),
LogDate date,
LogTime varchar (10),
IcecastUA_Key	varchar (50),
IPCountryISO	varchar (50),
IPCountry	varchar (50),
IPCity	varchar (50),
IPSubdivision	varchar (50),
IPSubdivisionIso	varchar (10),
NielsenId varchar (50)
);

create table gmedia.IcecastData10102018(
RequestIPAddress	varchar (20),
RequestTime	timestamp,
RequestType	varchar(10),
Station	varchar (100),
QueryTime	varchar (20),
Status varchar (10),
Lat float (10),
Lon	float (10),
Channel	varchar (50),
Kruxid	varchar (100),
adswizz	varchar (100),
PlayerId	varchar (50),
UserAgent varchar (300),	
Device varchar (100),
ListenerId	varchar (100),
ListenerId2	varchar (100),
Kuid	varchar (50),
skey	varchar (50),
Guid	varchar (100),
IcecastServer	varchar (50),
LogDate date,
LogTime varchar (10),
IcecastUA_Key	varchar (50),
IPCountryISO	varchar (50),
IPCountry	varchar (50),
IPCity	varchar (50),
IPSubdivision	varchar (50),
IPSubdivisionIso	varchar (10),
NielsenId varchar (50)
);

create table gmedia.IcecastStreamName(		
IcecastMountName varchar (100),
ServiceId varchar	(100),
StreamType varchar (100));

drop table gmedia.IcecastSongData;
create table gmedia.IcecastSongData(										
EpgEventsKey	varchar(100),
songStart	varchar (30),
songEnd	varchar(30),
ServiceId	varchar (100),
ID	varchar (100),
Duration varchar (30),
songType	varchar (50),
CartID	varchar (100),
Artists	varchar (100),
Title	varchar (100),
LogId	varchar (100),
Status varchar (20));


drop table gmedia.icecastshowdata;
create table gmedia.IcecastShowData(																	
EpgScheduleKey	varchar (100),
ServiceID	varchar(100),
ServiceName	varchar (100),
ServiceFqsn	varchar(100),
IncludeId	varchar (20),
data_From	timestamp,
data_To timestamp,	
Id	varchar (100),
data_Name	varchar (100),
Fqsn	varchar (150),
ScheduledId varchar (100),
InstanceId	varchar (100),
ShowId	varchar (100),
ShortName	varchar (100),
DefaultSynopsis varchar (300),
LogId varchar (100),
MediumName varchar (100),
LongName varchar (150)
);

drop table gmedia.TalkTalkclientTags;
create table gmedia.TalkTalkclientTags
(Time_Stamp timestamp,
IP_Address varchar (150),
URL varchar (300),
User_Agent varchar (300),
Query_String varchar (150),
Cookie varchar (300));

drop table gmedia.TalkTalkserverTags;
create table gmedia.TalkTalkserverTags
(Time_Stamp timestamp,
IP_Address varchar (150),
URL varchar (300),
User_Agent varchar (300),
Query_String varchar (300),
Cookie varchar (300));
