select customerid, delcodiscountvalueunitnet, salesdatetime from fleetcor.sales_item
where customerid=537 and priceruleid=1;


drop table fleetcor.sa_discount;
create table fleetcor.sa_discount as 
select 
b.toplevelcustomerid, 
b.startdate,
salesdatetime, 
quantity,
delcoretailpriceunitnet, 
delcodiscountvalueunitnet, 
priceruleid,
transactionguid, 
productgroupid
from fleetcor.sales_item a
join fleetcor.customer b on a.customerid= b.customerid
where productgroupid in (3,4)
group by b.toplevelcustomerid, 
b.startdate,
salesdatetime, 
quantity,
delcoretailpriceunitnet, 
delcodiscountvalueunitnet, 
priceruleid,
transactionguid, productgroupid
order by salesdatetime;


select avg from fleetcor.sa_discount
where toplevelcustomerid=494

select 
	a.customerid, 
	a.toplevelcustomerid,
	a.clientcustomernumber,
	b.priceruleid,
	b.dateeffective,
	b.dateterminated,
	c.value,
	c.priceruledescription
from fleetcor.customer a
join fleetcor.customer_price_rule b on  a.clientcustomernumber=b.clientcustomernumber
join fleetcor.price_rule_tier c on b.priceruleid=c.priceruleid
where toplevelcustomerid=496
group by 
	a.customerid, 
	a.toplevelcustomerid,
	a.clientcustomernumber,
	b.priceruleid,
	b.dateeffective,
	b.dateterminated,
	c.value,
	c.priceruledescription;
	
select count(distinct cardid)from fleetcor.sales_item_reduced
where customerid=496;

select delcodiscountvalueunitnet,priceruleid from fleetcor.sa_discount_unique
where delcodiscountvalueunitnet>0
group by delcodiscountvalueunitnet, priceruleid
;


select priceruleid, value, priceruledescription from fleetcor.price_rule_tier
where value>0.15 and productgrou;

select distinct priceruledescription
from fleetcor.price_rule_product
where
    product_group in ( 'Incentive Payment')
order by
    priceruledescription;

drop table fleetcor.sa_discount_unique_week_usage_rankq;

create table fleetcor.sa_discount_unique_week_usage_rankq as 
with agg as (
select 
	toplevelcustomerid, 
	min(startdate::date) as startdate,
	min(salesdatetime::date) as min_salesdate, 
	max(salesdatetime::date) as max_salesdate,
	count(distinct salesdatetime::date) as nr_of_active_days,
	delcodiscountvalueunitnet, 	
	productgroupid, 
	priceruleid, 
	sum(quantity) as total_quantity
from 
	fleetcor.sa_discount
group by
	toplevelcustomerid, 
	delcodiscountvalueunitnet, 	
	productgroupid,
	priceruleid
	)
select
	a.toplevelcustomerid, 
	startdate,
	min_salesdate, 
	max_salesdate,
	nr_of_active_days,
	delcodiscountvalueunitnet, 	
	productgroupid, 
	priceruleid, 
	total_quantity/nr_of_active_days as daily_quantity,
	b.rank_quantity
from agg a
left join fleetcor.customer_summ_master_2y b on a.toplevelcustomerid=b.top_level_customer_id;

	
drop table fleetcor.sa_discount_unique_ntile_adj;
--customer purchased quantity(sum of customers/quantity/unit_price_adjustment) bin view --
create table fleetcor.sa_discount_unique_ntile20_adj as 	

SELECT 
	count(distinct toplevelcustomerid) as customer_count, 
	avg(delcodiscountvalueunitnet) as discount, 
	avg(daily_quantity) as avg_quantity, 
	rank_quantity, 
	ntile(20) over (order by delcodiscountvalueunitnet) AS bin 
FROM fleetcor.sa_discount_unique_week_usage_rankq
group by delcodiscountvalueunitnet, rank_quantity
ORDER BY delcodiscountvalueunitnet;

create table fleetcor.sa_discount_unique_ntile_quantity as 	

create table fleetcor.sa_discount_unique_ntile_10bin as 

SELECT count(distinct toplevelcustomerid) as customer_count, 
avg(delcodiscountvalueunitnet) as discount, 
avg(weekly_quantity) as avg_quantity, 
ntile(10) over (order by delcodiscountvalueunitnet) AS bin 
FROM fleetcor.sa_discount_unique_ntile_adj
group by delcodiscountvalueunitnet
ORDER BY delcodiscountvalueunitnet;

--customer purchased quantity(sum of customers/quantity/unit_price_adjustment) 20 bin view --
create table fleetcor.sa_discount_unique_ntile_20bin as 

select sum(customer_count) customers, avg(avg_quantity) as quantity, avg(discount)as unit_price_adjustment from fleetcor.sa_discount_unique_ntile_adj
group by bin
order by bin;
	
select avg(sa_discount_unique.avg_quantity), ntile() within group (order by sa_discount_unique.delcodiscountvalueunitnet)
from fleetcor.sa_discount_unique
group by sa_discount_unique.delcodiscountvalueunitnet;

select count(distinct customerid), count(*) from fleetcor.customer;

select count(distinct toplevelcustomerid) from fleetcor.sa_test;

drop table fleetcor.sa_test;
create table fleetcor.sa_test as
select 
	b.toplevelcustomerid,
	a.quantity,
	b.startdate,
	a.delcodiscountvalueunitnet,
	count(distinct cardid) as card_count,
	a.salesdatetime,
	extract (year from salesdatetime::date)::int as sales_year,
	extract(week from salesdatetime::date)::int as sales_week_nr,
	(salesdatetime::date-startdate::date)/7 as week_dif,
	productgroupid, 
	customereffectivediscounttotalnet
from fleetcor.sales_item a
left join fleetcor.customer b on a.customerid=b.customerid
where startdate::date between '2016-09-01' and '2017-12-31' and a.productgroupid in (3,4)
group by 
	b.toplevelcustomerid,
	b.startdate,
	a.salesdatetime,
	a.quantity,
	a.delcodiscountvalueunitnet,
	productgroupid,
	customereffectivediscounttotalnet
	;

select count(distinct a.toplevelcustomerid), count(distinct c.cardid) 
from fleetcor.sa_test_churn a
join fleetcor.customer b on a.toplevelcustomerid=b.toplevelcustomerid
left join fleetcor.sales_item c on b.customerid=c.customerid
where segment='Key Corp Managed' and has_churned='Yes';

select 
		sum(quantity) as total_volume, 
		sum(delcodiscountvalueunitnet)+ sum(b.colcofeeamountnet) as total_revenue
from fleetcor.sa_test_segment a
left join fleetcor.sa_test_fee b on a.toplevelcustomerid=b.toplevelcustomerid and sales_year=b.fee_year and sales_week_nr=b.fee_week_nr
where segment='SME Unmanaged';


select week_dif, count(distinct toplevelcustomerid), sum(card_count) from fleetcor.sa_test
group by week_dif;

--adding fee column to the table
--sub query
drop table fleetcor.sa_test_fee;
create table fleetcor.sa_test_fee as 
select 
	b.customerid,	
	b.toplevelcustomerid,
	a.colcofeeamountnet,
	a.feedatetime,
	extract(year from feedatetime::date)::int as fee_year,
	extract(week from feedatetime::date)::int as fee_week_nr
from fleetcor.customer b
left join fleetcor.fee_item a  on a.customerid=b.customerid
where startdate::date between '2016-09-01' and '2017-12-31'
group by 
	b.toplevelcustomerid,
	b.customerid,
	a.colcofeeamountnet,
	a.feedatetime;

select count(distinct toplevelcustomerid) from fleetcor.sa_test_fee;

drop table fleetcor.sa_test_segment;
create table fleetcor.sa_test_segment as 
select a.*,
	b.segment
from fleetcor.sa_test a 
left join fleetcor.sa_segment_topcst b on a.toplevelcustomerid=b.toplevelcustomerid;


select count(distinct toplevelcustomerid) from fleetcor.sa_test_segment;

--lookup volume by segment
select  avg(quantity), count(distinct toplevelcustomerid), segment from fleetcor.sa_test_segment
group by segment;

--lookup volume by churn status
select avg(quantity), has_churned from fleetcor.sa_test_churn

select count (distinct toplevelcustomerid) from fleetcor.sa_test_sales_fee;

drop table fleetcor.sa_test_sales_fee;

create table fleetcor.sa_test_sales_fee as 
select 	
	a.customerid,	
	b.toplevelcustomerid,
	a.quantity,
	a.startdate,
	a.delcodiscountvalueunitnet,
	a.card_count,
	a.salesdatetime,
	sales_year,
	sales_week_nr,
	a.week_dif,
	a.productgroupid,
	b.colcofeeamountnet as fee_net,
	b.feedatetime,
	b.fee_year,
	b.fee_week_nr,
	a.segment
from  fleetcor.sa_test_segment a
join fleetcor.sa_test_fee b  on a.toplevelcustomerid=b.toplevelcustomerid and sales_year=b.fee_year and sales_week_nr=b.fee_week_nr
group by 
	a.customerid,	
	b.toplevelcustomerid,
	a.quantity,
	a.startdate,
	a.delcodiscountvalueunitnet,
	a.card_count,
	a.salesdatetime,
	sales_year,
	sales_week_nr,
	a.week_dif,
	a.productgroupid,
	b.colcofeeamountnet,
	b.feedatetime,
	b.fee_year,
	b.fee_week_nr,
	segment
;

drop table fleetcor.sa_segment_topcst;

create table fleetcor.sa_segment_topcst as 
select 
	a.toplevelcustomerid,
	b.tlid,
	b.accountnumber,
	b.accountname,
	b.segment
from   fleetcor.customer a 
join fleetcor.segment b  on a.toplevelcustomerid=b.tlid
group by a.toplevelcustomerid,
	b.tlid,
	b.accountnumber,
	b.accountname,
	b.segment;

select count(distinct toplevelcustomerid) from fleetcor.sa_segment_topcst;

where toplevelcustomerid is null;

--lookup price adjustmend average
select 
	week_dif, 
	avg(delcodiscountvalueunitnet) as price_adjust, 
	avg(quantity) 
from fleetcor.sa_test_sales_fee
group by week_dif;

--example case
select 
	a.toplevelcustomerid,
	a.sales_week_nr,
	a.week_dif,
	a.delcodiscountvalueunitnet,
	avg(quantity), productgroupid
from fleetcor.sa_test_churn_cut a
where toplevelcustomerid=134074
group by 
	toplevelcustomerid,
	week_dif,
	sales_week_nr,
	delcodiscountvalueunitnet, productgroupid
	;

--working with test case
drop table fleetcor.sa_test_case_rules;

create table fleetcor.sa_test_case_rules as 


select 
	a.toplevelcustomerid,	
	a.priceruleid, 
	a.priceruledescription, 
	a.dateeffective, 
	a.dateterminated, 
	b.dateeffective,
	b.value 
from fleetcor.customer_price_rule_topid a
join fleetcor.price_rule_tier b on a.priceruleid=b.priceruleid
where toplevelcustomerid=134074 and max(b.dateeffective);



select
	b.delcodiscountvalueunitnet,
	sum(value),
	b.salesdatetime, 
	b.priceruleid
from fleetcor.sa_test_case_rules a 
join fleetcor.sales_item b on a.toplevelcustomerid=b.customerid
where a.dateeffective between '2017-03-01' and '2017-04-30' and a.priceruleid not in (258,306,349) and b.salesdatetime between '2017-03-01' and '2017-04-30'
group by b.delcodiscountvalueunitnet, b.salesdatetime;



	
--adding the 'has_churned' flag	
drop table fleetcor.sa_test_churn;
create table fleetcor.sa_test_churn as 
select a.*,
	b.has_churned
from fleetcor.sa_test_segment a
left join fleetcor.customer_summ_master_2y b on a.toplevelcustomerid=b.top_level_customer_id;

drop table fleetcor.sa_test_churn_cut;
create table fleetcor.sa_test_churn_cut as 
select * from fleetcor.sa_test_churn
where startdate between '2016-09-01' and '2017-12-31';





select customerid, count(distinct card_count) from fleetcor.sa_test
where customerid=137131
group by customerid;


select count(distinct toplevelcustomerid) from fleetcor.sa_test_churn
where has_churned='Yes' and segment='Key Corp Managed';
--where has_churned='Yes';

--lookup churned in certain week
drop table fleetcor.sa_test_churned_each_week;

create table fleetcor.sa_test_churned_each_week as 

with agg as (
select 
	week_dif, 
	toplevelcustomerid,
	avg(delcodiscountvalueunitnet) as unit_price_increment,
	lead(week_dif) over (partition by toplevelcustomerid order by week_dif) as null_week
from fleetcor.sa_test_churn_cut
where has_churned= 'Yes' and segment ='SME Unmanaged'
group by week_dif, toplevelcustomerid
)
select week_dif, count(distinct toplevelcustomerid)
from agg
where null_week is null
group by week_dif;


select 	week_dif,
		count(distinct toplevelcustomerid),
		avg(delcodiscountvalueunitnet)
from fleetcor.sa_test_churn
where segment='SME Unmanaged' and has_churned='Yes'
group by week_dif;


select toplevelcustomerid from fleetcor.sa_test_churn
where segment='Key Corp Managed' and has_churned='Yes'
group by toplevelcustomerid;

select customerid, toplevelcustomerid from fleetcor.customer
where toplevelcustomerid=;

, 138939, 140639, 142482,143814

drop table fleetcor.sa_top_churn_segm;

create table fleetcor.sa_top_churn_segm as 
select a.toplevelcustomerid,
		a.has_churned,
		a.segment,
		b.customerid, 
		b.startdate
from fleetcor.sa_test_churn a
join fleetcor.customer b on a.toplevelcustomerid=b.toplevelcustomerid
where has_churned='Yes' and segment in ('SME Managed','SME Unmanaged','Key Corp Managed','Mid Corp Managed')
;

drop table fleetcor.sa_churned_card;
create table fleetcor.sa_churned_card as
select count(distinct a.cardid) as card,
	(a.salesdatetime::date-b.startdate::date)/7 as week_dif,
	b.toplevelcustomerid,
		b.has_churned,
		b.segment,
		b.customerid
from 
	fleetcor.sales_item a
join fleetcor.sa_top_churn_segm b on a.customerid=b.customerid
where b.startdate between '2016-09-01' and '2017-12-31'
group by week_dif, b.toplevelcustomerid, b.has_churned, b.segment, b.customerid
order by week_dif;

select count(card) from fleetcor.sa_churned_card
where customerid in (21540,28621,138553,139003,
139680,
141082,
145218,
136821,
144135)
;

select segment, card
from fleetcor.sa_churned_card 
where segment in ('SME Managed','SME Unmanaged','Key Corp Managed','Mid Corp Managed') and has_churned='Yes'
group by segment;

group by week_dif, toplevelcustomerid, card
order by week_dif;

select 
extract(year from salesdatetime::date)::int, 
	extract(week  from salesdatetime::date)::int
----getting the price average 

select 
	a.week_dif,
	sales_week_nr,
	sales_year
	(delcodiscountvalueunitnet) from fleetcor.sa_test_churn_cut
where has_churned='No' and segment in ('SME Managed','SME Unmanaged','Key Corp Managed','Mid Corp Managed')
group by week_dif;


--selecting the number of customers who churned/not
select count(distinct toplevelcustomerid) from fleetcor.sa_test_churn_cut
where has_churned='No' and segment in ('SME Managed','SME Unmanaged','Key Corp Managed','Mid Corp Managed')
;
--lookup for the final week
select week_dif, avg(delcodiscountvalueunitnet) from fleetcor.sa_test_churn_cut
where has_churned='No'
group by week_dif;

--lookup of fuel averages
select week_dif, avg(quantity)
from fleetcor.sa_test_churn_cut
where has_churned='No'and segment in ('SME Managed','SME Unmanaged','Key Corp Managed','Mid Corp Managed')
group by week_dif;

create table fleetcor.sa_test_churn_crd as 
select a.*,
	b.card_cnt_2y
from fleetcor.sa_test_churn_cut a
left join fleetcor.sa_test_crd_cnt b on a.toplevelcustomerid=b.toplevelcustomerid;

select segment, count(distinct toplevelcustomerid), sum(card_cnt_2y) from fleetcor.sa_test_churn_crd
where segment in ('SME Managed','SME Unmanaged','Key Corp Managed','Mid Corp Managed')
group by segment;

select toplevelcustomerid, segment, sum(card_cnt_2y) as numbe_of_cards from fleetcor.sa_test_churn_crd
where segment= 'Key Corp Managed' and has_churned='Yes'
group by toplevelcustomerid, segment;

select week_dif , avg(delcodiscountvalueunitnet) as price_per_unit_Mid_corp from fleetcor.sa_test_churn_crd
where segment='Mid Corp Managed'
group by week_dif;


with br as (
select distinct toplevelcustomerid , card_cnt_2y, segment
from fleetcor.sa_test_churn_crd
where has_churned='Yes' and segment in ('SME Managed','SME Unmanaged','Key Corp Managed','Mid Corp Managed') 
)
select sum(card_cnt_2y), segment from br
group by segment;

group by week_dif;


drop table fleetcor.sa_test_crd_cnt;
create table fleetcor.sa_test_crd_cnt as 
select 
	a.toplevelcustomerid, 
	b.card_cnt_2y
from fleetcor.sa_test_churn_cut a
join fleetcor.customer_summ_master_2y b on a.toplevelcustomerid=b.top_level_customer_id
group by a.toplevelcustomerid, b.card_cnt_2y;

select count(distinct toplevelcustomerid) from fleetcor.sa_test_crd_cnt
where card_cnt_2y is null;





--distinct price rule ids and description
with agg as (
select distinct priceruleid, priceruledescription, dateeffective, count(distinct toplevelcustomerid) as bravo
from fleetcor.customer_price_rule_topid
where priceruledescription like '%Delphi%' and dateeffective > '2016-09-01' and dateterminated< '2018-07-31'
group by dateeffective, priceruleid, priceruledescription
)
select priceruleid, priceruledescription,  sum(bravo) as customer_count
from agg
group by priceruleid, priceruledescription
order by priceruleid;

drop table fleetcor.sa_test_SME_Unma;

create table fleetcor.sa_test_SME_Unma as
select 
	distinct a.toplevelcustomerid, 
	a.has_churned,
	b.priceruleid, 
	b.priceruledescription,
	b.dateeffective,
	b.dateterminated
from fleetcor.sa_test_churn_cut a 
left join fleetcor.customer_price_rule_topid b on a.toplevelcustomerid=b.toplevelcustomerid
where segment='SME Unmanaged';

with delphi_customers as ( 
select distinct toplevelcustomerid
from fleetcor.sa_test_SME_Unma
where priceruledescription like '%Delphi%')


create table fleetcor.sa_test_cust_churn_nodelphi as
select 
	distinct toplevelcustomerid 
from fleetcor.sa_test_SME_Unma
where 
	has_churned='Yes' and toplevelcustomerid not in  
	(select distinct toplevelcustomerid
	from fleetcor.sa_test_SME_Unma
	where priceruledescription like '%Delphi%');

drop table fleetcor.sa_test_cust_nochurn_delphi_all;

create table fleetcor.sa_test_cust_nochurn_nodelphi_all as	
select 
	b.*
from  fleetcor.sa_test_cust_nochurn_nodelphi a 
join fleetcor.sa_test_churn_cut b on a.toplevelcustomerid=b.toplevelcustomerid;


select 
	week_dif,
	avg(delcodiscountvalueunitnet) as delco,
	avg(quantity) as quantity
from fleetcor.sa_test_cust_nochurn_delphi_all
group by week_dif
order by week_dif;


with prep as (
	select week_dif, 
		toplevelcustomerid,
		colcofeeamountnet,
		delcodiscountvalueunitnet,
		lead(week_dif) over (partition by toplevelcustomerid order by week_dif) as null_week
from fleetcor.sa_test_cust_churn_delphi_all_fee
group by toplevelcustomerid, week_dif, colcofeeamountnet, delcodiscountvalueunitnet)
select
	week_dif,
	avg(colcofeeamountnet) as fee, 
	avg(delcodiscountvalueunitnet) as delco,
	avg(colcofeeamountnet)+avg(delcodiscountvalueunitnet)as total_fees,
	count(distinct toplevelcustomerid) as cust_count
	from prep 
	where null_week is null
group by week_dif
order by week_dif;



with agg as (
select 
	week_dif, 
	toplevelcustomerid,
	avg(delcodiscountvalueunitnet) as unit_price_increment,
	lead(week_dif) over (partition by toplevelcustomerid order by week_dif) as null_week
from fleetcor.sa_test_churn_cut
where has_churned= 'Yes' and segment ='SME Unmanaged'
group by week_dif, toplevelcustomerid
)
select week_dif, count(distinct toplevelcustomerid)
from agg
where null_week is null
group by week_dif;






create table fleetcor.sa_test_cust_nochurn_nodelphi_all_fee as 
select 
	a.*, 
	b.colcofeeamountnet
from fleetcor.sa_test_cust_nochurn_nodelphi_all a
join fleetcor.sa_test_fee b on a.toplevelcustomerid=b.toplevelcustomerid and a.sales_year=b.fee_year and a.sales_week_nr=b.fee_week_nr;

create table fleetcor.sa_test_cust_churn_delphi_id as 
select a.toplevelcustomerid,
	b.customerid
from fleetcor.sa_test_cust_churn_delphi a 
left join fleetcor.customer b on a.toplevelcustomerid=b.toplevelcustomerid;

drop table fleetcor.sa_fee;
create table fleetcor.sa_fee as 
select a.*,
	b.feeruleid
from fleetcor.sa_test_fee a
join fleetcor.fee_item b on a.customerid=b.customerid and a.feedatetime=b.feedatetime;





select 
a.customerid, a.feeruleid, b.feeruledescription
from fleetcor.sa_fee a
join fleetcor.fee_rule_product b on a.feeruleid=b.feeruleid
group by a.customerid, a.feeruleid, b.feeruledescription;



create table fleetcor.sa_test_fee_table as 
select 
	b.toplevelcustomerid,
	a.customerid,
	a.feedatetime,
	a.feeruleid,
	a.originalfeeamountnet,
	a.colcofeeamountnet	
from fleetcor.fee_item a
join fleetcor.customer b on a.customerid=b.customerid;










with agg as(
select 
	week_dif, 
	(avg(customereffectivediscounttotalnet)+avg(colcofeeamountnet))::float as total_fee_paid_a_week
from fleetcor.sa_test_delphi
group by week_dif)
select week_dif, sum(total_fee_paid_a_week)
from agg 
group by week_dif
order by week_dif;

group by week_dif, customereffectivediscounttotalnet, colcofeeamountnet;

customereffectivediscounttotalnet

select 
	week_dif, 
	toplevelcustomerid,
	avg(colcofeeamountnet) 
from fleetcor.sa_test_cust_churn_nodelphi_all_fee
where colcofeeamountnet>20 and week_dif=1
group by week_dif, toplevelcustomerid
order by week_dif;


create table fleetcor.sa_test_churn_cut_weeks as 
select * 
from fleetcor.sa_test_churn_cut
where week_dif<=3;


drop table fleetcor.sa_test_cust_top99;
create table fleetcor.sa_test_cust_top99 as 
select distinct toplevelcustomerid
from fleetcor.sa_test_churn_cut
where segment in ('SME Managed', 'SME Unmanaged', 'Key Corp Managed', 'Mid Corp Managed');

drop table fleetcor.sa_test_cust_top99id;

create table fleetcor.sa_test_cust_top99id as 
select a.toplevelcustomerid,
	b.customerid, 
	b.startdate
from fleetcor.sa_test_cust_top99 a
join fleetcor.customer b on a.toplevelcustomerid=b.toplevelcustomerid
where b.startdate between '2016-09-01' and '2017-12-31';


drop table fleetcor.sa_test_cust_top99id_fee;
create table fleetcor.sa_test_cust_top99id_fee as 
select a.customerid, 
		a.toplevelcustomerid,
		b.feetypeid, 
		b.feeruleid, 
		b.feedatetime,
		b.colcofeeamountnet,
		(b.feedatetime::date - a.startdate::date)/7::int as week_nr
from fleetcor.sa_test_cust_top99id a
join fleetcor.fee_item b on a.customerid=b.customerid;

select 
	sum(colcofeeamountnet) as fee_amount_collected_total, 
	avg(colcofeeamountnet) as average_fee_amount_collected, 
	count(colcofeeamountnet) as fee_occurances, 
	feetypeid
from fleetcor.sa_test_cust_top99id_fee
where week_nr<=3
group by feetypeid;

select 
	a.feetypeid, a.feeruleid,
	sum(colcofeeamountnet) as sum_of_fee,
	count(distinct toplevelcustomerid) as occurrencies,
	b.feeruledescription
from fleetcor.sa_test_cust_top99id_fee a
join fleetcor.fee_rule_product b on a.feeruleid=b.feeruleid
where week_nr<=3 and feetypeid=42
group by a.feetypeid, a.feeruleid, b.feeruledescription;


select week_dif, feetypeid, sum(colcofeeamountnet)
from
fleetcor.sa_test_churn_cut_weeks_fees
where week_dif<=3
group by week_dif, feetypeid
order by week_dif;


create table fleetcor.sa_test_churn_cut_weeks_fees_desc as 
select 
	a.week_dif,
	a.feeruleid, 
	b.feeruledescription,
	b.value
from fleetcor.sa_test_churn_cut_weeks_fees a
join fleetcor.fee_rule_tier b on a.feeruleid=b.feeruleid
order by week_dif;

select count(*) from fleetcor.fee_rules;

select 
	count(*), feeruleid, feeruledescription
from fleetcor.sa_test_churn_cut_weeks_fees_desc
where week_dif<=3
group by feeruleid, feeruledescription, week_dif
order by week_dif;


drop table fleetcor.sa_test_fee_id;
create table fleetcor.sa_test_fee_id as 
select 
	a.customerid, 
	a.feeruleid,
	a.colcofeeamountnet,
	a.feedatetime,
	b.startdate
from fleetcor.fee_item a
join fleetcor.customer b on a.customerid=b.customerid
where a.customerid in (select toplevelcustomerid from fleetcor.sa_test_cust_churn_nodelphi_all_fee)
group by a.customerid, a.feeruleid, a.colcofeeamountnet, a.feedatetime, b.startdate
order by a.feedatetime;

select feeruleid,feeruledescription, value from fleetcor.fee_rule_tier
where feeruleid=6611
;



--SME Unamanaged Churn each week

with agg as (
select 
	week_dif, 
	toplevelcustomerid,
	lead(week_dif) over (partition by toplevelcustomerid order by week_dif) as null_week
from fleetcor.sa_test_cust_churn_nodelphi_all
)
select week_dif, count(distinct toplevelcustomerid) 
from agg
where null_week is null
group by week_dif;


select priceruleid, priceruledescription from fleetcor.customer_price_rule_topid
where toplevelcustomerid=134074;



select delcodiscountvalueunitnet, week_dif, has_churned
from fleetcor.sa_test_churn_cut
where toplevelcustomerid=134074
group by week_dif, delcodiscountvalueunitnet, has_churned;







create table fleetcor.sa_test_sample as	
select 
	a.toplevelcustomerid,
	a.quantity,
	a.startdate,
	a.delcodiscountvalueunitnet,
	a.card_count,
	a.salesdatetime,
	a.sales_week_nr,
	a.week_dif,
	a.productgroupid,
	b.fee_net
from fleetcor.sa_test a
left join fleetcor.sa_fee_by_week b on a.toplevelcustomerid=b.toplevelcustomerid and b.fee_week_nr=a.sales_week_nr
where a.toplevelcustomerid=2910 and week_dif between 1 and 12;


select toplevelcustomerid, week_dif,
	lead(week_dif) over (order by toplevelcustomerid, week_dif) as next_week_dif
from fleetcor.sa_test_sample;

select 
	a.*,
	b.dateeffective,
	b.dateterminated,
	b.priceruleid
from fleetcor.sa_test_sample a
join fleetcor.customer_price_rule_topid b on a.toplevelcustomerid=b.toplevelcustomerid
where a.salesdatetime::date between dateeffective::date and dateterminated::date;

create table fleetcor.customer_price_rule_topid as

select a.*,
	b.toplevelcustomerid
from fleetcor.customer_price_rule a
join fleetcor.customer b on a.clientcustomernumber=b.clientcustomernumber

select count(distinct toplevelcustomerid) from fleetcor.sa_test
where week_dif>=11;

select dateeffective, dateterminated, priceruledescription, priceruleid, toplevelcustomerid from fleetcor.customer_price_rule_topid
group by dateeffective, dateterminated, priceruledescription, priceruleid, toplevelcustomerid;



