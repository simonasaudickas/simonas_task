--------------------------------------------------------
-- AUTHOR: Simonas Audickas
-- EMAIL: simonas.audickas@beyndanalysis.net
-- CLIENT: Fleetcor
-- START DATE: 2018-09-20
--------------------------------------------------------
---Data used from the client:
	--- Customers;
	--- Sales_items;
	--- Fee_Items;
	--- Segments;
	--- Sites
	--- Customer Price Rule;
-- Data used from BA sources:
	---Customer Sic Clasiffication;
	--- Post-code look-up file;


-- Selecting the customer sales date for the last 12 m from Sales_item table provided by the client
drop table if exists fleetcor.sa_model;
create table fleetcor.sa_model as 
select 
	b.toplevelcustomerid,
	max(salesdatetime) as last_sales_date,
	count(transactionguid) as transaction_count,
	sum(quantity) as sum_volume,
	avg(quantity) as avg_volume,
	count(distinct cardid) as card_cnt,
	count(distinct siteid) as site_cnt,
	count(distinct fuelnetworkid) as network_cnt,
	max(customereffectivediscountunitnet) as max_unit_price_increment,
	avg(customereffectivediscountunitnet) as avg_total_unit_price,
	sum(customereffectivediscounttotalnet) as total_price_increment,
	count(distinct to_char(salesdatetime::date, 'yyyyww')) as active_weeks,
	sum(quantity)/count(distinct to_char(salesdatetime::date, 'yyyyww')) as average_fuel_per_week,
	1.00*count(transactionguid)/count(distinct to_char(salesdatetime::date, 'yyyyww')) as average_trans_aweek
from fleetcor.sales_item a
join fleetcor.customer b on a.customerid=b.customerid
where salesdatetime between '2017-07-31' and '2018-07-29' and productgroupid in (3,4)
group by toplevelcustomerid;




-- Extract of avg cards a week for 12 months
drop table if exists fleetcor.sa_model_avg_week_crd;
create table fleetcor.sa_model_avg_week_crd as 
with prep as (SELECT 
		b.toplevelcustomerid,
		to_char(salesdatetime::date, 'yyyy-iw') as week_id,
		count(distinct cardid) as cardid,
		count(distinct fuelnetworkid) as fuel_netw
				FROM fleetcor.sales_item a
	join fleetcor.customer b on a.customerid=b.customerid
	where salesdatetime between '2017-07-31' and '2018-07-29' and productgroupid in (3,4)
   	group by b.toplevelcustomerid, week_id
   	)
   	select toplevelcustomerid, 
   			avg(cardid) as avg_crd_aweek, 
   			avg(fuel_netw) as avg_fuel_netw
   	from prep
   	group by toplevelcustomerid;
   	
 
   
 drop table if exists fleetcor.sa_model_cst_size;
create table fleetcor.sa_model_cst_size as
select  
   				toplevelcustomerid,
				ntile(10) over (order by average_fuel_per_week)-1  as customer_size_score
FROM fleetcor.sa_model
group by toplevelcustomerid, average_fuel_per_week; 

   

----SIC code and division extract
drop table if exists fleetcor.sa_model_sic;
create table fleetcor.sa_model_sic as 
select 	b.toplevelcustomerid,
		c.sic_code,
		c.sic_division
from fleetcor.sales_item a
join fleetcor.customer b on a.customerid=b.customerid
join fleetcor.customer_sic_classification c on b.toplevelcustomerid=c.top_level_customer_id
where salesdatetime between '2017-07-31' and '2018-07-29' and productgroupid in (3,4)
group by b.toplevelcustomerid,
		c.sic_code,
		c.sic_division;



--Segmenting 12 month data into  bins on transaction count, avg week volume, network count, avg card count
drop table fleetcor.sa_model_perc;
create table fleetcor.sa_model_perc as 
with perc as (
	select
		a.toplevelcustomerid,
		a.average_fuel_per_week,
		a.average_trans_aweek,
		b.avg_crd_aweek,
		b.avg_fuel_netw		
	from fleetcor.sa_model a
	left join  fleetcor.sa_model_avg_week_crd b on a.toplevelcustomerid=b.toplevelcustomerid
	group by a.toplevelcustomerid,
		a.average_fuel_per_week,
		a.average_trans_aweek,
		b.avg_crd_aweek,
		b.avg_fuel_netw
		),
ska as (
select
		percentile_disc(0.90) within group (order by average_fuel_per_week asc)  as percentile_90, 
		percentile_disc(0.70) within group (order by average_fuel_per_week asc) as percentile_70,
		percentile_disc(0.90) within group (order by average_trans_aweek asc) as transactions_90, 
		percentile_disc(0.70) within group (order by average_trans_aweek asc) as transactions_70,
		percentile_disc(0.90) within group (order by avg_crd_aweek asc) as card_cnt_90, 
		percentile_disc(0.70) within group (order by avg_crd_aweek asc)  as card_cnt_70,
		percentile_disc(0.90) within group (order by avg_fuel_netw asc) as network_90, 
		percentile_disc(0.70) within group (order by avg_fuel_netw asc) as network_70
		from perc
	)
select	toplevelcustomerid,
		average_fuel_per_week,
		case 
		when average_fuel_per_week >= percentile_90 then 'high'
		when average_fuel_per_week >= percentile_70 and average_fuel_per_week < percentile_90 then 'medium'
		else 'low' 
	end as fuel_volume_cluster,
	average_trans_aweek,
	case 
		when average_trans_aweek>= transactions_90 then 'high'
		when average_trans_aweek >= transactions_70 and average_trans_aweek < transactions_90 then 'medium'
		else 'low' 
	end as transaction_cluster,
	avg_crd_aweek,
	case 
		when avg_crd_aweek >= card_cnt_90 then 'high'
		when avg_crd_aweek >= card_cnt_70 and avg_crd_aweek < card_cnt_90 then 'medium'
		else 'low' 
	end as card_cnt_cluster,
	avg_fuel_netw,
	case 
		when avg_fuel_netw >= network_90 then 'high'
		when avg_fuel_netw >= network_70 then 'medium'
		else 'low' 
	end as network_cluster
from perc, ska;


with high as(
	select max(average_trans_aweek) as  max_tr_high, min(average_trans_aweek) as min_tr_high, count(toplevelcustomerid) as cnt_tr_high, avg(average_trans_aweek)as av
	from fleetcor.sa_model_perc
	where transaction_cluster= 'high' ),
medium as (select max(average_trans_aweek) as max_tr_medium, min(average_trans_aweek) as min_tr_medium, count(toplevelcustomerid) as cnt_tr_medium
	from fleetcor.sa_model_perc
	where transaction_cluster= 'medium'
	),
low as (select max(average_trans_aweek)as max_tr_low, min(average_trans_aweek)as min_Tr_low, count(toplevelcustomerid) as cnt_tr_low
	from fleetcor.sa_model_perc
	where transaction_cluster= 'low'
	)
select av, max_tr_high,min_tr_high,cnt_tr_high,max_tr_medium,min_tr_medium, cnt_tr_medium, max_tr_low, min_tr_low, cnt_tr_low 
	from high, medium, low
	;

with high as(
	select max(avg_fuel_netw) as  max_tr_high, min(avg_fuel_netw) as min_tr_high, count(toplevelcustomerid) as cnt_tr_high, avg(avg_fuel_netw)as av
	from fleetcor.sa_model_perc
	where network_cluster= 'high' ),
medium as (select max(avg_fuel_netw) as max_tr_medium, min(avg_fuel_netw) as min_tr_medium, count(toplevelcustomerid) as cnt_tr_medium
	from fleetcor.sa_model_perc
	where network_cluster= 'medium'
	),
low as (select max(avg_fuel_netw)as max_tr_low, min(avg_fuel_netw)as min_Tr_low, count(toplevelcustomerid) as cnt_tr_low
	from fleetcor.sa_model_perc
	where network_cluster= 'low'
	)
select av, max_tr_high,min_tr_high,cnt_tr_high,max_tr_medium,min_tr_medium, cnt_tr_medium, max_tr_low, min_tr_low, cnt_tr_low 
	from high, medium, low
	;

with high as(
	select max(average_fuel_per_week) as  max_tr_high, min(average_fuel_per_week) as min_tr_high, count(toplevelcustomerid) as cnt_tr_high, avg(average_fuel_per_week)as av
	from fleetcor.sa_model_perc
	where fuel_volume_cluster= 'high' ),
medium as (select max(average_fuel_per_week) as max_tr_medium, min(average_fuel_per_week) as min_tr_medium, count(toplevelcustomerid) as cnt_tr_medium
	from fleetcor.sa_model_perc
	where fuel_volume_cluster= 'medium'
	),
low as (select max(average_fuel_per_week)as max_tr_low, min(average_fuel_per_week)as min_Tr_low, count(toplevelcustomerid) as cnt_tr_low
	from fleetcor.sa_model_perc
	where fuel_volume_cluster= 'low'
	)
select av, max_tr_high,min_tr_high,cnt_tr_high,max_tr_medium,min_tr_medium, cnt_tr_medium, max_tr_low, min_tr_low, cnt_tr_low 
	from high, medium, low
	;

	with high as(
	select max(avg_crd_aweek) as  max_tr_high, min(avg_crd_aweek) as min_tr_high, count(toplevelcustomerid) as cnt_tr_high, avg(avg_crd_aweek)as av
	from fleetcor.sa_model_perc
	where card_cnt_cluster= 'high' ),
medium as (select max(avg_crd_aweek) as max_tr_medium, min(avg_crd_aweek) as min_tr_medium, count(toplevelcustomerid) as cnt_tr_medium
	from fleetcor.sa_model_perc
	where card_cnt_cluster= 'medium'
	),
low as (select max(avg_crd_aweek)as max_tr_low, min(avg_crd_aweek)as min_Tr_low, count(toplevelcustomerid) as cnt_tr_low
	from fleetcor.sa_model_perc
	where card_cnt_cluster= 'low'
	)
select av, max_tr_high,min_tr_high,cnt_tr_high,max_tr_medium,min_tr_medium, cnt_tr_medium, max_tr_low, min_tr_low, cnt_tr_low 
	from high, medium, low

--Sales data for each customer each week to work out the last week numbers
drop table if exists	fleetcor.sa_model_sales_fee;
create table fleetcor.sa_model_sales_fee as 
with prep as(
	SELECT 
		b.toplevelcustomerid,
		sum(customereffectivediscounttotalnet) as total_disc,
		avg(customereffectivediscountunitnet) as delco_avg,
		sum(quantity) as week_volume,
		count(distinct cardid) as cards,
		to_char(salesdatetime::date, 'yyyy-iw') as week_id,
		count(distinct transactionguid) as trans_cnt
	FROM fleetcor.sales_item a
	join fleetcor.customer b on a.customerid=b.customerid
	where salesdatetime between '2017-07-31' and '2018-07-29' and productgroupid in (3,4)
   	group by b.toplevelcustomerid, week_id
   	),
step2 as (select toplevelcustomerid,
		row_number() over (partition by toplevelcustomerid order by week_id desc) as week_rank,
		total_disc,
		delco_avg,
		week_volume,
		cards,
		trans_cnt, week_id
		from prep),
 fees as(
	select 
		b.toplevelcustomerid,
		avg(colcofeeamountnet) as avg_fee,
		case 
			when count(cardid)=0 then 0
			else sum(colcofeeamountnet)/count(cardid) 
		end as fee_per_card,
		to_char(feedatetime::date, 'yyyy-iw') as fee_week_id
	from fleetcor.fee_item a
	join fleetcor.customer b on a.customerid=b.customerid
	where feedatetime between '2017-07-31' and '2018-07-29'
	group by b.toplevelcustomerid, fee_week_id),
	fee_step2 as (select toplevelcustomerid, avg_fee, fee_per_card, fee_week_id,
	row_number() over (partition by toplevelcustomerid order by fee_week_id desc) as fee_week_rank
	from fees)
select a.toplevelcustomerid, 
		week_rank,
		total_disc,
		delco_avg,
		week_volume,
		cards,
		case 
		when avg_fee is null then 0
			else total_disc+avg_fee 
			end as total_rev,
		trans_cnt, 
		week_id,
		avg_fee, 
		fee_per_card, 
		fee_week_id	
from step2 a
left join fee_step2 b on a.toplevelcustomerid=b.toplevelcustomerid and a.week_id=b.fee_week_id 
where week_rank=1; --and fee_week_rank=1;


--12 month data average comparison with the last week performance
drop table if exists fleetcor.sa_model_12m_lw_comp;
create table fleetcor.sa_model_12m_lw_comp as 
with prep as (
	select 	a.toplevelcustomerid,
			delco_avg - avg_total_unit_price as unit_price_increment,
			avg_total_unit_price,
			delco_avg,
			case 
				when avg_total_unit_price=0 then null
				else delco_avg/avg_total_unit_price-1 
			end as unit_price_increment_increase_perc,
			case
				when average_fuel_per_week=0 then null
				else 1.00*week_volume/ average_fuel_per_week-1
			end as volume_change_perc,
			week_volume - average_fuel_per_week as volume_change,
			week_volume,
			average_fuel_per_week,
			trans_cnt- average_trans_aweek as trans_count_change,
			average_trans_aweek, trans_cnt,
			trans_cnt/average_trans_aweek-1 as transaction_count_change_perc
		from fleetcor.sa_model a 
		join fleetcor.sa_model_sales_fee b on a.toplevelcustomerid=b.toplevelcustomerid
)
select toplevelcustomerid,
	case 
		when unit_price_increment>0 then 'Yes'
		else 'No'
		end as has_unit_price_increment_increase,
	case 
		when volume_change<0 then 'Yes'
		else 'No'
		end as has_volume_decrease,
	case
		when trans_count_change<0 then 'Yes'
		else 'No'
		end as has_transaction_count_decrease,
		unit_price_increment_increase_perc,
		unit_price_increment,
		volume_change_perc,
		week_volume,
		average_fuel_per_week,
		volume_change,
		transaction_count_change_perc,
		trans_cnt,
		average_trans_aweek,
		trans_count_change
from prep;



-----Customer segment data table 	
create table fleetcor.sa_model_segment as 
select b.toplevelcustomerid,
		c.segment
from fleetcor.sales_item a 
join fleetcor.customer b on a.customerid=b.customerid
join fleetcor.segment c on b.toplevelcustomerid=c.tlid
where salesdatetime between '2017-07-31' and '2018-07-29' and productgroupid in (3,4)
group by b.toplevelcustomerid, c.segment;


	
-- Regional flag added to the Model table--
drop table if exists fleetcor.sa_model_region_flag;
create table fleetcor.sa_model_region_flag as 
with aga as(
select 
	d.toplevelcustomerid,
	count(c.transactionguid) as transaction_count,
	b.district	
from fleetcor.sites a 
join fleetcor.sales_item c on a.siteid::int=c.siteid::int
join assets.postcode_lookup b on a.zipcode=b.postcode
join fleetcor.customer d on c.customerid=d.customerid
where c.salesdatetime between '2017-07-31' and '2018-07-29' and productgroupid in (3,4)
group by b.district, d.toplevelcustomerid)
select
	toplevelcustomerid, 
	case
		when count(distinct district)> 5 and max(transaction_count)/sum(transaction_count)*100 <75 then 'national'
		when max(transaction_count)/sum(transaction_count)*100 >90 then 'local'
		else 'regional'
		end as regional_flag
	from aga
	group by toplevelcustomerid;
	
	
---Tenure data ---
drop table if exists fleetcor.sa_model_tenure;
create table fleetcor.sa_model_tenure as 
with cust as (
			select 
					b.toplevelcustomerid,
					age('2018-07-29',min( b.startdate)::date) as tenure_general
			from fleetcor.sales_item a
			join fleetcor.customer b on a.customerid=b.customerid
			where salesdatetime between '2017-07-31' and '2018-07-29' and productgroupid in (3,4)
			group by b.toplevelcustomerid),
lasting as (
			select toplevelcustomerid,
			(extract(year from tenure_general)*12)::int+ extract(month from tenure_general)::int as tenure_months
			from cust)
select toplevelcustomerid, 
			case 
	when tenure_months <12 then '0-1y'
	when tenure_months < 24then '1-2y'
	when tenure_months <60 then '2-5y'
	else '5y+'
	end as tenure
	from lasting
	group by toplevelcustomerid, tenure_months;

	
-- Segmenting Total revenue and Unit price incremen data into bins
drop table fleetcor.sa_model_sales_clust;
create table fleetcor.sa_model_sales_clust as 
with sales_clust as (
	select
		toplevelcustomerid, 
		total_disc,
		delco_avg,
		week_volume,
		cards,	
		total_rev,
		trans_cnt, 
		avg_fee, 
		fee_per_card 
		from fleetcor.sa_model_sales_fee
	group by toplevelcustomerid, 
		total_disc,
		delco_avg,
		week_volume,
		cards,	
		total_rev,
		trans_cnt, 
		avg_fee, 
		fee_per_card 
		),
ska as (
select
		percentile_disc(0.90) within group (order by total_rev asc)  as total_rev_90, 
		percentile_disc(0.80) within group (order by total_rev asc) as total_rev_80,
		percentile_disc(0.90) within group (order by delco_avg asc) as delco_90, 
		percentile_disc(0.70) within group (order by delco_avg asc) as delco_70
	from sales_clust
	)
select	toplevelcustomerid,
	case 
		when total_rev >= total_rev_90 then 'high'
		when total_rev >= total_rev_80 and total_rev < total_rev_90 then 'medium'
		else 'low' 
	end as total_rev_cluster,
	case 
		when delco_avg >= delco_90 then 'high'
		when delco_avg >= delco_70 and delco_avg < delco_90 then 'medium'
		else 'low' 
	end as delco_cluster,
		total_disc as last_week_unit_price_increment,
		delco_avg as last_week_avg_unit_price_increment,
		week_volume as last_week_volume,
		cards as last_week_card_cnt,	
		total_rev as last_week_total_revenue,
		trans_cnt as last_week_trans_cnt, 
		avg_fee as last_week_avg_fee, 
		fee_per_card as last_week_fee_per_card,
		delco_90, delco_70
from ska, sales_clust
	group by toplevelcustomerid, total_rev_90, total_rev_80,total_disc,
		delco_avg,
		week_volume,
		cards,	
		total_rev,
		trans_cnt, 
		avg_fee, 
		fee_per_card,
	delco_90,
	delco_70;
	

with high as(
	select max( last_week_total_revenue) as  max_tr_high, min( last_week_total_revenue) as min_tr_high, count(toplevelcustomerid) as cnt_tr_high
	from  fleetcor.sa_model_sales_clust
	where tdelco_cluster= 'high' ),
medium as (select max( last_week_total_revenue) as max_tr_medium, min(  last_week_total_revenue) as min_tr_medium, count(toplevelcustomerid) as cnt_tr_medium
	from fleetcor.sa_model_sales_clust
	where delco_cluster= 'medium'
	),
low as (select max(  last_week_total_revenue)as max_tr_low, min(last_week_total_revenue)as min_Tr_low, count(toplevelcustomerid) as cnt_tr_low
	from fleetcor.sa_model_sales_clust
	where total_rev_cluster= 'low'
	)
select max_tr_high,min_tr_high,cnt_tr_high,max_tr_medium,min_tr_medium, cnt_tr_medium, max_tr_low, min_tr_low, cnt_tr_low 
	from high, medium, low	
	
with high as(
	select max( last_week_avg_unit_price_increment) as  max_tr_high, min( last_week_avg_unit_price_increment) as min_tr_high, count(toplevelcustomerid) as cnt_tr_high
	from  fleetcor.sa_model_sales_clust
	where delco_cluster= 'high' ),
medium as (select max(last_week_avg_unit_price_increment) as max_tr_medium, min(  last_week_avg_unit_price_increment) as min_tr_medium, count(toplevelcustomerid) as cnt_tr_medium
	from fleetcor.sa_model_sales_clust
	where delco_cluster= 'medium'
	),
low as (select max(last_week_avg_unit_price_increment)as max_tr_low, min(last_week_avg_unit_price_increment)as min_Tr_low, count(toplevelcustomerid) as cnt_tr_low
	from fleetcor.sa_model_sales_clust
	where delco_cluster= 'low'
	)
select max_tr_high,min_tr_high,cnt_tr_high,max_tr_medium,min_tr_medium, cnt_tr_medium, max_tr_low, min_tr_low, cnt_tr_low 
	from high, medium, low
	
--- Check if RBP applied the last week of sales----
drop table if exists fleetcor.sa_model_rbp_lw;
create table fleetcor.sa_model_rbp_lw as 
with week_prep as (
	SELECT 
		toplevelcustomerid,
		last_sales_date
	FROM fleetcor.sa_model
	group by toplevelcustomerid, last_sales_date),
date_check as (
	select a.toplevelcustomerid, 
		b.priceruledescription,
		case
			when b.priceruledescription like '%Delphi%' and max(last_sales_date)::date between max(b.dateeffective) and max(b.dateterminated) then 1
			else 0
		end as rbp
	from week_prep a
		left join fleetcor.customer_price_rule_topid b on a.toplevelcustomerid=b.toplevelcustomerid
		group by a.toplevelcustomerid, priceruledescription)
select toplevelcustomerid,
		case
			when max(rbp)>0 then 'Yes'
			else 'No'
			end as has_risk_based_prising
		from date_check
		group by toplevelcustomerid;
		
	

---FULL TABLE JOIN--
drop table if exists fleetcor.sa_model_all;
create table fleetcor.sa_model_all as 
select 	a.toplevelcustomerid as top_level_customer_id,
		b.sic_code,
		b.sic_division,
		d.tenure,
		a.last_sales_date as last_fuel_sales_date,
				-----size indicators------
		g.segment as fleetcor_segment,
		f.regional_flag as coverage_flag,
		e.fuel_volume_cluster as fuel_volume_segment_l12m,
		e.transaction_cluster as transaction_count_segment_l12m,
		e.card_cnt_cluster as card_count_segment_l12m,
		e.network_cluster as network_count_segment_l12m,
		---retention metrics-----
		h.attrition_rank as attrition_propensity_score,
		z.customer_size_score,
		k.has_risk_based_prising,
		j.has_volume_decrease,
		j.has_unit_price_increment_increase,
		j.has_transaction_count_decrease,
		j.volume_change_perc,
		j.transaction_count_change_perc,
		j.unit_price_increment_increase_perc,
		j.volume_change,
		j.trans_count_change as transaction_count_change,
		j.unit_price_increment as unit_price_increment_increase,
		----behaviour last 12 months ------
		a.active_weeks as active_week_count_l12m,
		a.transaction_count as transaction_count_l12m,
		j.average_trans_aweek as avg_transaction_count_l12m,
		a.sum_volume as total_fuel_volume_l12m,
		a.avg_volume as avg_fuel_volume_l12m,
		a.average_fuel_per_week as avg_fuel_volume_per_active_week,
		a.card_cnt as active_card_cnt_l12m,
		i.avg_crd_aweek as average_card_count_per_active_week,
		a.site_cnt as site_cnt_l12m,
		a.network_cnt as network_cnt_l12m,
		a.max_unit_price_increment as max_unit_price_increment_l12m,
		a.avg_total_unit_price as avg_unit_price_increment_l12m ,
		a.total_price_increment as total_price_increment_l12m,
				-----behaviour last full week-----
		c.total_rev_cluster as total_revenue_segment_lw,
		c.delco_cluster as price_increment_segment_lw,
		c.last_week_unit_price_increment as total_unit_price_increment_lw,
		c.last_week_avg_unit_price_increment as avg_unit_price_increment_lw,
		c.last_week_volume as volume_lw,
		c.last_week_card_cnt as card_count_lw,	
		c.last_week_total_revenue as total_revenue_lw,
		c.last_week_trans_cnt as trans_count_lw, 
		c.last_week_avg_fee as avg_fee_lw, 
		c.last_week_fee_per_card as fee_per_card_lw,
		l."sum" as fee_total_l12m,
		(a.total_price_increment/a.active_weeks) as total_price_increment_per_active_week,
		(l."sum" /a.card_cnt) as fees_per_active_card_l12m
from fleetcor.sa_model a
left join fleetcor.sa_model_sic b on a.toplevelcustomerid=b.toplevelcustomerid
left join fleetcor.sa_model_sales_clust c on b.toplevelcustomerid=c.toplevelcustomerid
left join fleetcor.sa_model_tenure d on c.toplevelcustomerid=d.toplevelcustomerid
left join fleetcor.sa_model_perc e on d.toplevelcustomerid=e.toplevelcustomerid
left join fleetcor.sa_model_region_flag f on e.toplevelcustomerid=f.toplevelcustomerid
left join fleetcor.sa_model_segment g on f.toplevelcustomerid=g.toplevelcustomerid
left join fleetcor.mk_attrition h on g.toplevelcustomerid=h.toplevelcustomerid
left join fleetcor.sa_model_avg_week_crd i on a.toplevelcustomerid=i.toplevelcustomerid
left join fleetcor.sa_model_12m_lw_comp j on a.toplevelcustomerid=j.toplevelcustomerid
left join fleetcor.sa_model_rbp_lw k on a.toplevelcustomerid=k.toplevelcustomerid
left join fleetcor.sa_model_cst_size z on a.toplevelcustomerid=z.toplevelcustomerid
left join fleetcor.sa_model_fee_total l on a.toplevelcustomerid=l.toplevelcustomerid;




drop table if exists fleetcor.sa_model_all_scores_all;

drop table if exists fleetcor.sa_model_all_scores_all;
create table fleetcor.sa_model_all_scores_all as 
with gen_tbl as (select 	a.toplevelcustomerid as top_level_customer_id,
		b.sic_code,
		b.sic_division,
		d.tenure,
		a.last_sales_date as last_fuel_sales_date,
				-----size indicators------
		g.segment as fleetcor_segment,
		f.regional_flag as coverage_flag,
		e.fuel_volume_cluster as fuel_volume_segment_l12m,
		e.transaction_cluster as transaction_count_segment_l12m,
		e.card_cnt_cluster as card_count_segment_l12m,
		e.network_cluster as network_count_segment_l12m,
		---retention metrics-----
		h.attrition_rank as attrition_propensity_score,
		z.customer_size_score,
		k.has_risk_based_prising,
		j.has_volume_decrease,
		j.has_unit_price_increment_increase,
		j.has_transaction_count_decrease,
		j.volume_change_perc,
		j.transaction_count_change_perc,
		j.unit_price_increment_increase_perc,
		j.volume_change,
		j.trans_count_change as transaction_count_change,
		j.unit_price_increment as unit_price_increment_increase,
		----behaviour last 12 months ------
		a.active_weeks as active_week_count_l12m,
		a.transaction_count as transaction_count_l12m,
		j.average_trans_aweek as avg_transaction_count_l12m,
		a.sum_volume as total_fuel_volume_l12m,
		a.avg_volume as avg_fuel_volume_l12m,
		a.average_fuel_per_week as avg_fuel_volume_per_active_week,
		a.card_cnt as active_card_cnt_l12m,
		i.avg_crd_aweek as average_card_count_per_active_week,
		a.site_cnt as site_cnt_l12m,
		a.network_cnt as network_cnt_l12m,
		a.max_unit_price_increment as max_unit_price_increment_l12m,
		a.avg_total_unit_price as avg_unit_price_increment_l12m ,
		a.total_price_increment as total_price_increment_l12m,
				-----behaviour last full week-----
		c.total_rev_cluster as total_revenue_segment_lw,
		c.delco_cluster as price_increment_segment_lw,
		c.last_week_unit_price_increment as total_unit_price_increment_lw,
		c.last_week_avg_unit_price_increment as avg_unit_price_increment_lw,
		c.last_week_volume as volume_lw,
		c.last_week_card_cnt as card_count_lw,	
		c.last_week_total_revenue as total_revenue_lw,
		c.last_week_trans_cnt as trans_count_lw, 
		c.last_week_avg_fee as avg_fee_lw, 
		c.last_week_fee_per_card as fee_per_card_lw,
		l."sum" as fee_total_l12m,
		(a.total_price_increment/a.active_weeks) as total_price_increment_per_active_week,
		(l."sum" /a.card_cnt) as fees_per_active_card_l12m,
		m.lead1,
		m.lead2
from fleetcor.sa_model a
left join fleetcor.sa_model_sic b on a.toplevelcustomerid=b.toplevelcustomerid
left join fleetcor.sa_model_sales_clust c on b.toplevelcustomerid=c.toplevelcustomerid
left join fleetcor.sa_model_tenure d on c.toplevelcustomerid=d.toplevelcustomerid
left join fleetcor.sa_model_perc e on d.toplevelcustomerid=e.toplevelcustomerid
left join fleetcor.sa_model_region_flag f on e.toplevelcustomerid=f.toplevelcustomerid
left join fleetcor.sa_model_segment g on f.toplevelcustomerid=g.toplevelcustomerid
left join fleetcor.mk_attrition h on g.toplevelcustomerid=h.toplevelcustomerid
left join fleetcor.sa_model_avg_week_crd i on a.toplevelcustomerid=i.toplevelcustomerid
left join fleetcor.sa_model_12m_lw_comp j on a.toplevelcustomerid=j.toplevelcustomerid
left join fleetcor.sa_model_rbp_lw k on a.toplevelcustomerid=k.toplevelcustomerid
left join fleetcor.sa_model_cst_size z on a.toplevelcustomerid=z.toplevelcustomerid
left join fleetcor.sa_model_fee_total l on a.toplevelcustomerid=l.toplevelcustomerid
left join fleetcor.sa_model_lead m on a.toplevelcustomerid= m.toplevelcustomerid),
smeunmanaged as (select *, 
	case
    when avg_fuel_volume_per_active_week < 90 then 1
    when avg_fuel_volume_per_active_week < 250 then 2
    when avg_fuel_volume_per_active_week < 500 then 3
    else 4
end as fuel_score,
case
    when site_cnt_l12m < 11 then 1
    when site_cnt_l12m < 31 then 2
    when site_cnt_l12m < 101 then 3
    else 4
end as site_score,
case
    when total_price_increment_per_active_week < 0 then 1
    when total_price_increment_per_active_week = 0 then 2
    when total_price_increment_per_active_week < 20 then 3
    else 4
end as price_increment_score,
case
    when active_card_cnt_l12m < 3 then 1
    when active_card_cnt_l12m < 6 then 2
    when active_card_cnt_l12m < 13 then 3
    else 4
end as card_score,
case
    when fee_total_l12m = 0 then 1
    when fee_total_l12m < 100 then 2
    when fee_total_l12m < 500 then 3
    else 4
end as fees_score,
case
when tenure ='0-1y' then 1
    when  tenure ='1-2y'  then 2
    when  tenure ='2-5y'  then 3
    else 4
    end as tenure_score
from gen_tbl
where fleetcor_segment = 'SME Unmanaged'
), 
smemanaged as (
select *, 
	case
    when avg_fuel_volume_per_active_week < 500 then 1
    when avg_fuel_volume_per_active_week < 1250 then 2
    when avg_fuel_volume_per_active_week < 2000 then 3
    else 4
end as fuel_score,
case
    when site_cnt_l12m < 7 then 1
    when site_cnt_l12m < 22 then 2
    when site_cnt_l12m < 58 then 3
    else 4
end as site_score,
case
    when total_price_increment_per_active_week < -65 then 1
    when total_price_increment_per_active_week < -14 then 2
    when total_price_increment_per_active_week < 0 then 3
    else 4
end as price_increment_score,
case
    when active_card_cnt_l12m < 14 then 1
    when active_card_cnt_l12m < 32 then 2
    when active_card_cnt_l12m < 59 then 3
    else 4
end as card_score,
case
    when fee_total_l12m < 794 or fee_total_l12m is null then 1
    when fee_total_l12m < 2678 then 2
    when fee_total_l12m < 5234 then 3
    else 4
end as fees_score,
case
when tenure ='0-1y' then 1
    when  tenure ='1-2y'  then 2
    when  tenure ='2-5y'  then 3
    else 4
    end as tenure_score
from gen_tbl
where fleetcor_segment = 'SME Managed'
),
midcorpmanaged as (
select *, 
	case
    when avg_fuel_volume_per_active_week < 950 then 1
    when avg_fuel_volume_per_active_week < 3500 then 2
    when avg_fuel_volume_per_active_week < 7300 then 3
    else 4
end as fuel_score,
case
    when site_cnt_l12m < 134 then 1
    when site_cnt_l12m < 458 then 2
    when site_cnt_l12m < 1109 then 3
    else 4
end as site_score,
case
    when total_price_increment_per_active_week < -213 then 1
    when total_price_increment_per_active_week < -27 then 2
    when total_price_increment_per_active_week < 0 then 3
    else 4
end as price_increment_score,
case
    when active_card_cnt_l12m < 27 then 1
    when active_card_cnt_l12m < 112 then 2
    when active_card_cnt_l12m < 233 then 3
    else 4
end as card_score,
case
    when fee_total_l12m < 695 or fee_total_l12m is null then 1
    when fee_total_l12m < 2779 then 2
    when fee_total_l12m < 7234 then 3
    else 4
end as fees_score,
case
when tenure ='0-1y' then 1
    when  tenure ='1-2y'  then 2
    when  tenure ='2-5y'  then 3
    else 4
    end as tenure_score
from gen_tbl
where fleetcor_segment = 'Mid Corp Managed'),
keycorpmanaged as (
select *, 
	case
    when avg_fuel_volume_per_active_week < 150 then 1
    when avg_fuel_volume_per_active_week < 500 then 2
    when avg_fuel_volume_per_active_week < 2000 then 3
    else 4
end as fuel_score,
case
    when site_cnt_l12m < 32 then 1
    when site_cnt_l12m < 101 then 2
    when site_cnt_l12m < 363 then 3
    else 4
end as site_score,
case
    when total_price_increment_per_active_week < -40 then 1
    when total_price_increment_per_active_week < 0  then 2
    when total_price_increment_per_active_week = 0 then 3
    else 4
end as price_increment_score,
case
    when active_card_cnt_l12m < 8 then 1
    when active_card_cnt_l12m < 22then 2
    when active_card_cnt_l12m < 85 then 3
    else 4
end as card_score,
case
    when fee_total_l12m < 64 or fee_total_l12m is null then 1
    when fee_total_l12m < 438 then 2
    when fee_total_l12m < 1420 then 3
    else 4
end as fees_score,
case
when tenure ='0-1y' then 1
    when  tenure ='1-2y'  then 2
    when  tenure ='2-5y'  then 3
    else 4
    end as tenure_score
from gen_tbl
where fleetcor_segment = 'Key Corp Managed'),
other as (
select *, 
	case
    when avg_fuel_volume_per_active_week < 80 then 1
    when avg_fuel_volume_per_active_week < 250 then 2
    when avg_fuel_volume_per_active_week < 800 then 3
    else 4
end as fuel_score,
case
    when site_cnt_l12m < 6 then 1
    when site_cnt_l12m < 38 then 2
    when site_cnt_l12m < 168 then 3
    else 4
end as site_score,
case
    when total_price_increment_per_active_week < -10 then 1
    when total_price_increment_per_active_week < 0 then 2
    when total_price_increment_per_active_week = 0 then 3
    else 4
end as price_increment_score,
case
    when active_card_cnt_l12m < 3 then 1
    when active_card_cnt_l12m < 6 then 2
    when active_card_cnt_l12m < 13 then 3
    else 4
end as card_score,
case
    when fee_total_l12m < 20 or fee_total_l12m is null then 1
    when fee_total_l12m < 114 then 2
    when fee_total_l12m < 404 then 3
    else 4
end as fees_score,
case
when tenure ='0-1y' then 1
    when  tenure ='1-2y'  then 2
    when  tenure ='2-5y'  then 3
    else 4
    end as tenure_score
from gen_tbl
where fleetcor_segment  not in ('SME Unmanaged', 'SME Managed', 'Mid Corp Managed', 'Key Corp Managed') or fleetcor_segment is null)
select	*,
		case 
			when has_risk_based_prising='Yes' then 'rbp'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=11 then 'good'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=14 then 'better'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=18 then 'best'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=24 then 'vip'
			end as client_score_segment
from smeunmanaged
union all
select	*,
		case 
			when has_risk_based_prising='Yes' then 'rbp'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=15 then 'good'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=18 then 'better'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=21 then 'best'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=24 then 'vip'
			end as client_score_segment
from smemanaged
union all
select	*,
		case 
			when has_risk_based_prising='Yes' then 'rbp'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=13 then 'good'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=17 then 'better'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=20 then 'best'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=24 then 'vip'
			end as client_score_segment
from midcorpmanaged
union all
select	*,
		case 
			when has_risk_based_prising='Yes' then 'rbp'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=13 then 'good'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=16 then 'better'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=19 then 'best'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=24 then 'vip'
			end as client_score_segment
from keycorpmanaged
union all
select	*,
		case 
			when has_risk_based_prising='Yes' then 'rbp'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=12 then 'good'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=16 then 'better'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=20 then 'best'
			when fuel_score+site_score+ price_increment_score+card_score+fees_score+tenure_score<=24 then 'vip'
			end as client_score_segment
from other;

